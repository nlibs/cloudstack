provider "cloudstack" {
  api_url    = "http://cloudstack1.supernovagate.internal:8080/client/api"
  api_key    = "cQg6pQi8xVMfQ3e9UBJ7bIqTIRAnCXct6fRX7kS8c-wcSBPwJBxv0bxkt1OxuX5FziXhKo0xFh5sspQSvqUk0Q"
  secret_key = "P-uLJfolVbGl5T3wL64k4SNT11MwJP4fUoGar4IQ5eKc0prH7LDz8zhm43cxYLPAhr3ba2xcMVYd4nMa-fmgdg"
}

resource "cloudstack_network" "templating-net" {
  name             = "templating-net"
  cidr             = "10.1.20.0/24"
  network_offering = "DefaultIsolatedNetworkOfferingForVpcNetworks"
  zone             = "home"
  display_text   = "Templating Network"
  network_domain   = "mystorm.cloud"
  vpc_id   = "${cloudstack_vpc.vpc-templating.id}"
}

resource "cloudstack_vpc" "vpc-templating" {
  name         = "vpc-templating"
  cidr         = "10.1.20.0/24"
  vpc_offering = "Default VPC Offering"
  zone         = "home"
  network_domain   = "mystorm.cloud"
}