#!/bin/bash
cli=cloudmonkey
sourcetmpl_name=ubuntu-plexmedia-16.04-latest
computeoffer_name="XX-Large Instance"
computeostype_name="Ubuntu 16.04 (64-bit)"
rootdisk_size=100
dns_servicename="plexmedia"
zone_name=home
network_displaytext="Plex Media Server Production"
network_name="plexmedia-prod"
network_offeringname="DefaultSharedNetworkOffering"
network_vlan=50
network_startip=192.168.50.10
network_endip=192.168.50.100
network_gateway=192.168.50.1
network_netmask=255.255.255.0
networkacl_name=default_allow
network_domain=mystorm.cloud

#Routine to add timestap to output messages
log(){
  echo $(date +%d-%m-%Y" "%H:%M:%S) $1
}

addDNS() {
    cat <<EOF | nsupdate
zone $network_domain
update add $1.$network_domain 60 A $2
send
EOF
}

deleteDNS() {
    cat <<EOF | nsupdate
zone $network_domain
update delete $1.$network_domain. A
send
EOF
}

addTag() {
$cli createTags resourceids=$1 resourcetype=userVm tags[0].key=application tags[0].value=$dns_servicename 2>&1 >/dev/null
}

#Start Processing
clear
echo "**************************************************************"
echo "**********        Plexmedia Server Recycle Host    ************"
echo "**************************************************************"
echo 
echo
echo "**********      Environment Configuration    ******************"
echo

log "Getting Cloud Environment Parameters"
zone_id=`$cli listZones name=$zone_name | grep ^id\ = | awk '{print $3}'`
tmpl_id=`$cli listTemplates templatefilter=all name=$sourcetmpl_name | grep ^id\ = | awk '{print $3}'`
computeoffer_id=`$cli listServiceOfferings name="$computeoffer_name" | grep ^id\ = | awk '{print $3}'`
networkoffer_id=`$cli listNetworkOfferings name=$network_offeringname | grep ^id\ = | awk '{print $3}'`
key_id=`$cli listSSHKeyPairs name=$key_name | grep ^id\ = | awk '{print $3}'`
network_aclid=`$cli listNetworkACLLists name=$networkacl_name | grep ^id\ = | awk '{print $3}'`

log "Checking if Network $network_name exists"
network_id=`$cli listNetworks keyword=$network_name | grep ^id\ = | awk '{print $3}'`
if [ -z "${network_id}" ]; then
	log "Network $network_name not configured"
    log "Creating Network $network_name"
    network_id=`$cli createNetwork startip=$network_startip endip=$network_endip vlan=$network_vlan displaytext=$network_displaytext networkdomain=$network_domain aclid=$network_aclid netmask=$network_netmask gateway=$network_gateway name=$network_name networkofferingid=$networkoffer_id zoneid=$zone_id | grep ^id\ = | awk '{print $3}'`
	else 
	log "Network Ready - No Action Required"	
fi     

echo
echo "**********      Environment Configured    ******************"
echo
echo "**********      Recycling Instances   ******************"	

log "Checking if any $dns_servicename instances are running"
	runninginstance_id=`$cli listVirtualMachines tags[0].key=application tags[0].value=$dns_servicename | grep ^id\ = | awk '{print $3}'`
	existinginstance=1
if [ -z "${runninginstance_id}" ]; then	
	log "No $dns_servicename instances running"
	existinginstance=0
else
	runninginstance_name=`$cli listVirtualMachines id=$runninginstance_id | grep ^name\ = | awk '{print $3}'`
	
fi	

log "Launching New Latest Instance"
instanceid=`$cli deployVirtualMachine rootdisksize=$rootdisk_size networkids=$network_id serviceofferingid=$computeoffer_id templateid=$tmpl_id keypair=$key_id zoneid=$zone_id | grep ^id\ = | awk '{print $3}'`

log "Tagging Application as $dns_servicename"
addTag $instanceid

log "Waiting for Instance to come online"
while [ "$ready" = Running ]; do ready=`$cli listVirtualMachines id=$instanceid | grep ^state\ = | awk '{print $3}'`; sleep 1; done
log "Instance Running"

sleep 30

publicip=`$cli listVirtualMachines id=$instanceid | grep ^ipaddress\ = | awk '{print $3}'`

log "Updating DNS Service Name with Instance IP"
	deleteDNS $dns_servicename
	addDNS $dns_servicename $publicip

log 

if [ $existinginstance = 1 ]; then
log "Found instance $runninginstance_name deployed"
	log "Destroying instance $runninginstance_name"
	$cli stopVirtualMachine id=$runninginstance_id 2>&1 >/dev/null
	$cli destroyVirtualMachine id=$runninginstance_id expunge=true 2>&1 >/dev/null
fi
echo
echo "**************************************************************"
echo "********     Plexmedia Service Recycled Successfully ******"
echo "**************************************************************"



