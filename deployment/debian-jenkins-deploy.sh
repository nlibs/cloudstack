#!/bin/bash
cli=cloudmonkey
sourcetmpl_name=debian-jenkins-9.5-latest
computeoffer_name="Medium Instance"
computeostype_name="Other Linux (64-bit)"
rootdisk_size=100
dns_servicename="jenkins"
zone_name=home
VPC_cidr_range=10.1.70.0/24
VPC_name="jenkins-prod"
VPC_displaytext="VPC for Jenkins service"
VPC_offeringname="Default VPC offering"
network_displaytext="Jenkins Production"
network_name="jenkins-prod-net"
network_offeringname="DefaultIsolatedNetworkOfferingForVpcNetworks"
network_gateway=10.1.70.1
network_netmask=255.255.255.0
networkacl_name=default_allow
network_domain=mystorm.cloud
lb_name=lb_jenkins
lb_port=8080
lb_algorithm=source
lb_description="Jenkins Loadbalancer"

#Routine to add timestap to output messages
log(){
  echo $(date +%d-%m-%Y" "%H:%M:%S) $1
}

addDNS() {
    cat <<EOF | nsupdate
zone $network_domain
update add $1.$network_domain 60 A $2
send
EOF
}

deleteDNS() {
    cat <<EOF | nsupdate
zone $network_domain
update delete $1.$network_domain. A
send
EOF
}

addTag() {
$cli createTags resourceids=$1 resourcetype=userVm tags[0].key=application tags[0].value=$dns_servicename >/dev/null
}

#Start Processing
clear
echo "**************************************************************"
echo "**********        Jenkins Recycle Host       ************"
echo "**************************************************************"
echo 
echo
echo "**********      Environment Configuration    ******************"
echo

log "Getting Cloud Environment Parameters"
zone_id=`$cli listZones name=$zone_name | grep ^id\ = | awk '{print $3}'`
tmpl_id=`$cli listTemplates templatefilter=all name=$sourcetmpl_name | grep ^id\ = | awk '{print $3}'`
computeoffer_id=`$cli listServiceOfferings name="$computeoffer_name" | grep ^id\ = | awk '{print $3}'`
networkoffer_id=`$cli listNetworkOfferings name=$network_offeringname | grep ^id\ = | awk '{print $3}'`
VPC_offeringid=`$cli listVPCOfferings name=$VPC_offeringname | grep ^id\ = | awk '{print $3}'`
key_id=`$cli listSSHKeyPairs name=$key_name | grep ^id\ = | awk '{print $3}'`
network_aclid=`$cli listNetworkACLLists name=$networkacl_name | grep ^id\ = | awk '{print $3}'`

VPC_id=`$cli listVPCs name=$VPC_name | grep ^id\ = | awk '{print $3}'`

log "Checking if VPC $VPC_name exists"
if [ -z "${VPC_id}" ]; then
	log "VPC $VPC_name not configured"
    log "Creating VPC $VPC_name"
    VPC_id=`$cli createVPC cidr=$VPC_cidr_range displaytext="$VPC_displaytext" networkdomain=$network_domain name=$VPC_name vpcofferingid=$VPC_offeringid zoneid=$zone_id | grep ^id\ = | awk '{print $3}'`
	else
	log "VPC Ready - No Action Required"	
fi

log "Checking if Network $network_name exists"
network_id=`$cli listNetworks vpcid=$VPC_id | grep ^id\ = | awk '{print $3}'`
if [ -z "${network_id}" ]; then
	log "Network $network_name not configured"
    log "Creating Network $network_name"
    network_id=`$cli createNetwork displaytext=$network_displaytext networkdomain=$network_domain aclid=$network_aclid netmask=$network_netmask gateway=$network_gateway name=$network_name networkofferingid=$networkoffer_id zoneid=$zone_id vpcid=$VPC_id | grep ^id\ = | awk '{print $3}'`
	else 
	log "Network Ready - No Action Required"	
fi     

log "Checking if Public IP is associated"
publicip_id=`$cli listPublicIpAddresses associatednetworkid=$network_id issourcenat=false | grep ^id\ = | awk '{print $3}'`
if [ -z "${publicip_id}" ]; then
	log "Public IP not reserved"
	log "Reserving Public IP in Network Tier"
	publicip_id=`$cli associateIpAddress vpcid=$VPC_id | grep ^id\ = | awk '{print $3}'`
	publicip=`$cli listPublicIpAddresses id=$publicip_id | grep ^ipaddress\ = | awk '{print $3}'`
	else
	log "Public IP Reserved - No Action Required"	
	publicip=`$cli listPublicIpAddresses id=$publicip_id | grep ^ipaddress\ = | awk '{print $3}'`
fi

log "Updating DNS for Loadbalancer IP"
	deleteDNS $dns_servicename
	addDNS $dns_servicename $publicip

log "Checking if Loadbalancer has been created"
	lb_id=`$cli listLoadBalancerRules networkid=$network_id publicipid=$publicip_id | grep ^id\ = | awk '{print $3}'`
if [ -z "${lb_id}" ]; then
	log "Loadbalancer $lb_name not configured"
	log "Creating Load $lb_name"
	lb_id=`$cli createLoadBalancerRule algorithm=$lb_algorithm privateport=$lb_port publicport=$lb_port name=$lb_name networkid=$network_id publicipid=$publicip_id description=$lb_description | grep ^id\ = | awk '{print $3}'`
else 
	log "Loadbalancer Ready - No Action Required"	
fi   

echo
echo "**********      Environment Configured    ******************"
echo
echo "**********      Recycling Instances   ******************"

log "Checking if any instances are running in VPC"
	runninginstance_id=`$cli listVirtualMachines details=min networkid=$network_id | grep ^id\ = | awk '{print $3}'`
if [ -z "${runninginstance_id}" ]; then	
	log "No instances running in VPC"
else
	runninginstance_name=`$cli listVirtualMachines id=$runninginstance_id | grep ^name\ = | awk '{print $3}'`
	log "Found instance $runninginstance_name deployed"
fi	

log "Launching New Latest Instance"
instanceid=`$cli deployVirtualMachine rootdisksize=$rootdisk_size networkids=$network_id serviceofferingid=$computeoffer_id templateid=$tmpl_id keypair=$key_id zoneid=$zone_id | grep ^id\ = | awk '{print $3}'`

log "Tagging Application as $dns_servicename"
addTag $instanceid

log "Waiting for Instance to come online"
while [ "$ready" = Running ]; do ready=`$cli listVirtualMachines id=$instanceid | grep ^state\ = | awk '{print $3}'`; sleep 1; done
log "Instance Running"

log "Assigning Instance to loadbalancer"
	$cli assignToLoadBalancerRule id=$lb_id virtualmachineids=$instanceid 2>&1 >/dev/null

if ! [ -z "${runninginstance_id}" ]; then	
	log "Removing Previous instance $runninginstance_name from Loadbalancer"
	$cli removeFromLoadBalancerRule id=$lb_id virtualmachineids=$runninginstance_id 2>&1 >/dev/null
	log "Destroying Previous Instance"
	$cli destroyVirtualMachine id=$runninginstance_id expunge=true >&1 >/dev/null
fi

echo
echo "**************************************************************"
echo "********     Jenkins Service Recycled Successfully      ******"
echo "**************************************************************"



