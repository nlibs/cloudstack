#!/bin/bash
cli=cloudmonkey
sshkey_file=~/code/cloudstack/keys/cloudstack.pem
ansible_playbook=ubuntu-basebuild.yml
ansible_folder=~/code/cloudstack/ansible/
sourcetmpl_name=ubuntu-server-16.04.5-gold
targettmpl_displaytext="Ubuntu Server 16.04 LTS"
targettmpl_name="ubuntu-server-16.04-latest"
previoustmpl_name="ubuntu-server-16.04-previous"
computeoffer_name="Small Instance"
computeostype_name="Ubuntu 16.04 (64-bit)"
compute_rootdisksize=50
zone_name=home
key_name=ssh-keypair
VPC_cidr_range=10.1.20.0/24
VPC_name="vpc-templating"
VPC_displaytext="Template creation VPC - Temporary"
VPC_offeringname="Default VPC offering"
network_displaytext="Templating Network - Temporary"
network_name="templating-net"
network_domain=mystorm.cloud
network_offeringname="DefaultIsolatedNetworkOfferingForVpcNetworks"
network_gateway=10.1.20.1
network_netmask=255.255.255.0
networkacl_name=default_allow

#Functions
#Wait Routine for instance to become available via SSH
sssh(){
  while true; do command ssh -o "StrictHostKeyChecking no" "$@" 2>&1 >/dev/null; [ $? -eq 0 ] && break || sleep 0.5; done  
}

#Routine to add timestap to output messages
log(){
  echo $(date +%d-%m-%Y" "%H:%M:%S) $1
}

#Start Processing
clear
echo "**************************************************************"
echo "**********    Ubuntu Standard Template Creation   ************"
echo "**************************************************************"
echo 
echo
echo "**********      Environment Configuration    ******************"
echo
log "Getting Cloud Environment Parameters"
zone_id=`$cli listZones name=$zone_name | grep ^id\ = | awk '{print $3}'`
tmpl_id=`$cli listTemplates templatefilter=all name=$sourcetmpl_name | grep ^id\ = | awk '{print $3}'`
computeoffer_id=`$cli listServiceOfferings name="$computeoffer_name" | grep ^id\ = | awk '{print $3}'`
networkoffer_id=`$cli listNetworkOfferings name=$network_offeringname | grep ^id\ = | awk '{print $3}'`
VPC_offeringid=`$cli listVPCOfferings name=$VPC_offeringname | grep ^id\ = | awk '{print $3}'`
key_id=`$cli listSSHKeyPairs name=$key_name | grep ^id\ = | awk '{print $3}'`
network_aclid=`$cli listNetworkACLLists name=$networkacl_name | grep ^id\ = | awk '{print $3}'`

log "**Checking if VPC $VPC_name exists"
VPC_id=`$cli listVPCs name=$VPC_name | grep ^id\ = | awk '{print $3}'`
if [ -z "${VPC_id}" ]; then
	log "VPC $VPC_name not configured"
    log "Creating VPC $VPC_name"
    VPC_id=`$cli createVPC cidr=$VPC_cidr_range displaytext="$VPC_displaytext" networkdomain=$network_domain name=$VPC_name vpcofferingid=$VPC_offeringid zoneid=$zone_id | grep ^id\ = | awk '{print $3}'`
	else
	log "VPC Ready - No Action Required"	
fi

log "**Checking if Network $network_name exists"
network_id=`$cli listNetworks vpcid=$VPC_id | grep ^id\ = | awk '{print $3}'`
if [ -z "${network_id}" ]; then
	log "Network $network_name not configured"
    log "Creating Network $network_name"
    network_id=`$cli createNetwork displaytext=$network_displaytext networkdomain=$network_domain aclid=$network_aclid netmask=$network_netmask gateway=$network_gateway name=$network_name networkofferingid=$networkoffer_id zoneid=$zone_id vpcid=$VPC_id | grep ^id\ = | awk '{print $3}'`
	else 
	log "Network Ready - No Action Required"	
fi     

log "**Checking if Public IP is associated"
publicip_id=`$cli listPublicIpAddresses associatednetworkid=$network_id issourcenat=false isstaticnat=false | grep ^id\ = | awk '{print $3}'`
if [ -z "${publicip_id}" ]; then
	log "Public IP not reserved"
	log "Reserving Public IP in Network Tier"
	publicip_id=`$cli associateIpAddress vpcid=$VPC_id | grep ^id\ = | awk '{print $3}'`
	publicip=`$cli listPublicIpAddresses id=$publicip_id | grep ^ipaddress\ = | awk '{print $3}'`
	else
	log "Public IP Reserved - No Action Required"	
	publicip=`$cli listPublicIpAddresses id=$publicip_id | grep ^ipaddress\ = | awk '{print $3}'`
fi

echo
echo "**********      Environment Configured    ******************"
echo
echo "**********      Preparing Template Host   ******************"
log "Starting up host in VPC"
instanceid=`$cli deployVirtualMachine networkids=$network_id serviceofferingid=$computeoffer_id templateid=$tmpl_id keypair=$key_id zoneid=$zone_id | grep ^id\ = | awk '{print $3}'`
volume_id=`$cli listVolumes virtualmachineid=$instanceid | grep ^id\ = | awk '{print $3}'`

log "Associate static NAT with Host"
$cli enableStaticNat ipaddressid=$publicip_id virtualmachineid=$instanceid networkid=$network_id >/dev/null

log "Waiting for VM to complete Linux OS startup"
sssh -t -i $sshkey_file root@"$publicip" 'exit' 2>&1 >/dev/null
sleep 30
log "Host running and ready for configuration"

log "Adding host IP entry to Ansible Hosts"
sed -i '' '/template/  a\
'$publicip' \
' /etc/ansible/hosts

log "Run Playbook $ansible_playbook"
ansible-playbook -b $ansible_folder$ansible_playbook -u root --private-key $sshkey_file 

#2>&1 >/dev/null

echo
echo "**********      Template Host Configured     ******************"
echo
echo "**********      Starting Templating sequence ******************"
echo
log "Shutting down host for templating"
$cli stopVirtualMachine id=$instanceid 2>&1 >/dev/null
log "Waiting for VM to stop"
while [ "$state" != "Stopped" ]; do state=`$cli listVirtualMachines id=$instanceid | grep ^state\ = | awk '{print $3}'`; sleep 0.5; done
log "VM stopped"

log "Demote previous latest and delete deprecated template"
previoustmpl_id=`$cli listTemplates templatefilter=all name=$targettmpl_name | grep ^id\ = | awk '{print $3}'`
deprecated_id=`$cli listTemplates templatefilter=all name=$previoustmpl_name | grep ^id\ = | awk '{print $3}'`

if ! [ -z $deprecated_id ]; then
$cli deleteTemplate id=$deprecated_id 2>&1 >/dev/null
fi	

if ! [ -z $previoustmpl_id ]; then
$cli updateTemplate id=$previoustmpl_id name=$previoustmpl_name	2>&1 >/dev/null
fi

log "Create latest template"
computeostype_id=`$cli listOsTypes description="$computeostype_name" | grep ^id\ = | awk '{print $3}'`
newtmpl_id=`$cli createTemplate displaytext="$targettmpl_displaytext" volumeid=$volume_id name="$targettmpl_name" ostypeid=$computeostype_id ispublic=true isfeatured=true | grep ^id\ = | awk '{print $3}'`

# log "Waiting for template to be created"
# while [ "$ready" = True ]; do ready=`$cli listTemplates id=$newtmpl_id | grep ^isready\ = | awk '{print $3}'`; sleep 1; done
# log "Template ready"

echo
echo "**********      Template Ready and Uploaded  ******************"
echo
echo "**********      Removing Template Host       ******************"
echo

log "Removing StaticNat and release public IP"
$cli disableStaticNat ipaddressid=$publicip_id 2>&1 >/dev/null
$cli disassociateIpAddress id=$publicip_id 2>&1 >/dev/null

log "Destroying source VM"
$cli destroyVirtualMachine id=$instanceid expunge=true 2>&1 >/dev/null

log "Remove entries from Ansible hosts"
sudo sed -i '' "/$publicip/d" /etc/ansible/hosts
echo
echo "**************************************************************"
echo "********     Ubuntu Standard Image Created Successfully ******"
echo "**************************************************************"
