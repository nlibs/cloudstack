#!/bin/bash
cli=cloudmonkey
sshkey_file=~/code/cloudstack/keys/cloudstack.pem
packer_folder=~/code/cloudstack/imaging/
packer_file=debian-jenkins-image.json
sourcetmpl_name=debian-server-9.5-latest
targettmpl_displaytext="Debian 9.5 Jenkins Server"
targettmpl_name="debian-9.5-jenkins-latest"
previoustmpl_name="debian-9.5-jenkins-latest"
zone_name=home
key_name=ssh-keypair
VPC_cidr_range=10.1.20.0/24
VPC_name="vpc-templating"
VPC_displaytext="Template creation VPC"
VPC_offeringname="Default VPC offering"
network_displaytext="Templating Network"
network_name="templating-net"
network_domain=mystorm.cloud
network_offeringname="DefaultIsolatedNetworkOfferingForVpcNetworks"
network_gateway=10.1.20.1
network_netmask=255.255.255.0
networkacl_name=default_allow

#Functions

#Routine to add timestap to output messages
log(){
  echo $(date +%d-%m-%Y" "%H:%M:%S) $1
}

clear

log "Getting Cloud Environment Parameters"
zone_id=`$cli listZones name=$zone_name | grep ^id\ = | awk '{print $3}'`
tmpl_id=`$cli listTemplates templatefilter=all name=$sourcetmpl_name | grep ^id\ = | awk '{print $3}'`
computeoffer_id=`$cli listServiceOfferings name="$computeoffer_name" | grep ^id\ = | awk '{print $3}'`
networkoffer_id=`$cli listNetworkOfferings name=$network_offeringname | grep ^id\ = | awk '{print $3}'`
VPC_offeringid=`$cli listVPCOfferings name=$VPC_offeringname | grep ^id\ = | awk '{print $3}'`
key_id=`$cli listSSHKeyPairs name=$key_name | grep ^id\ = | awk '{print $3}'`
network_aclid=`$cli listNetworkACLLists name=$networkacl_name | grep ^id\ = | awk '{print $3}'`
VPC_id=`$cli listVPCs name=$VPC_name | grep ^id\ = | awk '{print $3}'`

log "Checking if VPC $VPC_name exists"
if [ -z "${VPC_id}" ]; then
	log "VPC $VPC_name not configured"
    log "Creating VPC $VPC_name"
    VPC_id=`$cli createVPC cidr=$VPC_cidr_range displaytext="$VPC_displaytext" networkdomain=$network_domain name=$VPC_name vpcofferingid=$VPC_offeringid zoneid=$zone_id | grep ^id\ = | awk '{print $3}'`
	else
	log "VPC Ready - No Action Required"	
fi

log "Checking if Network $network_name exists"
network_id=`$cli listNetworks vpcid=$VPC_id | grep ^id\ = | awk '{print $3}'`
if [ -z "${network_id}" ]; then
	log "Network $network_name not configured"
    log "Creating Network $network_name"
    network_id=`$cli createNetwork displaytext=$network_displaytext networkdomain=$network_domain aclid=$network_aclid netmask=$network_netmask gateway=$network_gateway name=$network_name networkofferingid=$networkoffer_id zoneid=$zone_id vpcid=$VPC_id | grep ^id\ = | awk '{print $3}'`
	else 
	log "Network Ready - No Action Required"	
fi  

log "Demote previous latest and delete deprecated template"
previoustmpl_id=`$cli listTemplates templatefilter=all name=$targettmpl_name | grep ^id\ = | awk '{print $3}'`
deprecated_id=`$cli listTemplates templatefilter=all name=$previoustmpl_name | grep ^id\ = | awk '{print $3}'`

if ! [ -z $deprecated_id ]; then
$cli deleteTemplate id=$deprecated_id 2>&1 >/dev/null
fi	

if ! [ -z $previoustmpl_id ]; then
$cli updateTemplate id=$previoustmpl_id name=$previoustmpl_name	2>&1 >/dev/null
fi   

~/code/cloudstack/packer/packer build "$packer_folder"/"$packer_file"






