cli=cloudmonkey
zoneid=`$cli list zones | grep ^id\ = | awk '{print $3}'`
PhyNetName="Physical Network 2"

phy_id2=`$cli list physicalnetworks name=$PhyNetName zoneid=$zoneid | grep ^id\ = | awk '{print $3}'
echo $phy_id2`
nsp_id=`$cli list networkserviceproviders name=VpcVirtualRouter physicalnetworkid=$phy_id2 | grep ^id\ = | awk '{print $3}'`
vre_id=`$cli list virtualrouterelements nspid=$nsp_id | grep ^id\ = | awk '{print $3}'`
$cli api configureVirtualRouterElement enabled=true id=$vre_id
$cli update networkserviceprovider state=Enabled id=$nsp_id
echo "Enabled virtual router element and network service provider"


