#!/bin/bash
 
cli=cloudmonkey
dns_ext=8.8.8.8
dns_int=192.168.10.1
gw=192.168.20.1
nmask=255.255.255.0
hpvr=kvm
pod_start=192.168.20.50
pod_end=192.168.20.99
vlan_start=192.168.20.100
vlan_end=192.168.20.199
 
#Put space separated host ips in following
host_ips=192.168.20.13
host_user=root
host_passwd=N0tS3cur3
sec_storage=nfs://192.168.20.11/disk1/secondary
prm_storage=nfs://192.168.20.11/disk1/primary
 
zone_id=`$cli create zone dns1=$dns_ext internaldns1=$dns_int name=Zone_home networktype=Advanced securitygroupenabled=1 | grep ^id\ = | awk '{print $3}'`
echo "Created zone" $zone_id
 
phy_id1=`$cli create physicalnetwork name=mgmt-network zoneid=$zone_id | grep ^id\ = | awk '{print $3}'`
phy_id2=`$cli create physicalnetwork name=guest-network zoneid=$zone_id | grep ^id\ = | awk '{print $3}'`
echo "Created physical Management network" $phy_id1
echo "Created physical Guest network" $phy_id2
$cli add traffictype traffictype=Guest physicalnetworkid=$phy_id2 kvmnetworklabel=cloudbr1 vlan=30
echo "Added guest traffic"
$cli add traffictype traffictype=Management physicalnetworkid=$phy_id1 kvmnetworklabel=cloudbr0
echo "Added mgmt traffic"
$cli update physicalnetwork state=Enabled id=$phy_id1
echo "Enabled physicalnetwork mgmt-network"
$cli update physicalnetwork state=Enabled id=$phy_id2
echo "Enabled physicalnetwork guest-network"
 
nsp_id=`$cli list networkserviceproviders name=VirtualRouter physicalnetworkid=$phy_id1 | grep ^id\ = | awk '{print $3}'`
vre_id=`$cli list virtualrouterelements nspid=$nsp_id | grep ^id\ = | awk '{print $3}'`
$cli api configureVirtualRouterElement enabled=true id=$vre_id
$cli update networkserviceprovider state=Enabled id=$nsp_id
echo "Enabled virtual router element and network service provider"

nsp_id=`$cli list networkserviceproviders name=VirtualRouter physicalnetworkid=$phy_id2 | grep ^id\ = | awk '{print $3}'`
vre_id=`$cli list virtualrouterelements nspid=$nsp_id | grep ^id\ = | awk '{print $3}'`
$cli api configureVirtualRouterElement enabled=true id=$vre_id
$cli update networkserviceprovider state=Enabled id=$nsp_id
echo "Enabled virtual router element and network service provider"
 
nsp_sg_id=`$cli list networkserviceproviders name=SecurityGroupProvider vlan=30 physicalnetworkid=$phy_id2 | grep ^id\ = | awk '{print $3}'`
$cli update networkserviceprovider state=Enabled id=$nsp_sg_id
echo "Enabled security group provider"
 
netoff_id=`$cli list networkofferings name=DefaultSharedNetworkOfferingWithSGService | grep ^id\ = | awk '{print $3}'`
net_id=`$cli create network zoneid=$zone_id name=DefaultIsolatedNetworkOfferingForVpcNetworksWithInternalLB displaytext=DefaultIsolatedNetworkOfferingForVpcNetworksWithInternalLB networkofferingid=$netoff_id | grep ^id\ = | awk '{print $3}'`
echo "Created network $net_id for zone" $zone_id
 
pod_id=`$cli create pod name=MyPod zoneid=$zone_id gateway=$gw netmask=$nmask startip=$pod_start endip=$pod_end | grep ^id\ = | awk '{print $3}'`
echo "Created pod"
 
$cli create vlaniprange podid=$pod_id networkid=$net_id gateway=$gw netmask=$nmask startip=$vlan_start endip=$vlan_end forvirtualnetwork=false
echo "Created IP ranges for instances"
 
cluster_id=`$cli add cluster zoneid=$zone_id hypervisor=$hpvr clustertype=CloudManaged podid=$pod_id clustername=MyCluster | grep ^id\ = | awk '{print $3}'`
echo "Created cluster" $cluster_id
 
#Put loop here if more than one
for host_ip in $host_ips;
do
  $cli add host zoneid=$zone_id podid=$pod_id clusterid=$cluster_id hypervisor=$hpvr username=$host_user password=$host_passwd url=http://$host_ip;
  echo "Added host" $host_ip;
done;
 
$cli create storagepool zoneid=$zone_id podid=$pod_id clusterid=$cluster_id name=MyNFSPrimary url=$prm_storage
echo "Added primary storage"
 
$cli add secondarystorage zoneid=$zone_id url=$sec_storage
echo "Added secondary storage"
 
$cli update zone allocationstate=Enabled id=$zone_id
echo "Basic zone deloyment completed!"



