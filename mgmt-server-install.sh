echo "deb http://cloudstack.apt-get.eu/ubuntu xenial 4.11" > /etc/apt/sources.list.d/cloudstack.list
wget -O - http://cloudstack.apt-get.eu/release.asc|apt-key add -
apt-get update
apt-get install uuid-runtime -y
apt-get install cloudstack-management -y
apt-get install mysql-server -y

echo '[mysqld]' >> /etc/mysql/my.cnf 
echo "server-id=master-01" >> /etc/mysql/my.cnf
echo "innodb_rollback_on_timeout=1" >> /etc/mysql/my.cnf
echo "innodb_lock_wait_timeout=600" >> /etc/mysql/my.cnf
echo "max_connections=700" >> /etc/mysql/my.cnf
echo "log-bin=mysql-bin" >> /etc/mysql/my.cnf
echo "binlog-format = 'ROW'" >> /etc/mysql/my.cnf
echo "bind-address = 0.0.0.0" >> /etc/mysql/my.cnf

cloudstack-setup-databases cloud:@localhost --deploy-as=root:N0tS3cur3
cloudstack-setup-management
mkdir /disk1
mount -t nfs cloud_storage:/disk1 /disk1
/usr/share/cloudstack-common/scripts/storage/secondary/cloud-install-sys-tmplt -m /disk1/secondary -u http://cloudstack.apt-get.eu/systemvm/4.11/systemvmtemplate-4.11.0-kvm.qcow2.bz2 -h kvm -F
chmod 777 /disk1 -R





