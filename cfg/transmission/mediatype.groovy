//Does a check of inut file and writes JSON with response


def media=args.getFiles{ it.isVideo() || it.isSubtitle() }
media=media[0]

mediaout=this.args[1]

  	File mediaout = new File("$mediaout")
     
 	def tvs = detectSeriesName(media)
 	def mov = (parseEpisodeNumber(media) || parseDate(media)) ? null : detectMovie(media, false) 

	mediaout.write "{\"series\":\"$tvs\",\"movie\":\"$mov\"}"
