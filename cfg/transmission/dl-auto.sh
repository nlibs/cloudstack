#!/bin/bash

TR_TORRENT_DIR="/mnt/bt/incoming" #Transmisson incoming dir
WORKING_DIR="/mnt/bt/dl-manager" # Directory where scripts are located
TARGET_DIR="/mnt/itunes"	#Target Directory for output files
TRANS_AUTH='bt:T0rr3ntz.' #Transmission RPC credentials
TVDB_USERNAME="nlindblo" #Username for thetvdb API
TVDB_APIKEY="0F95BD71AA33AADC" #API Key for thetvdb API
TVDB_USERKEY="5307F491F13D6518" #User Key for thetvdb API
###################################################################
DEBUG=0
TRANSMISSION_USER=`whoami`
JAVA_DIR=`which java`

TR_DOWNLOADS="$TR_TORRENT_DIR"/"$TR_TORRENT_NAME"

function edate {
	echo "`date '+%Y-%m-%d %H:%M:%S'`    $1" >> "$LOGFILE"
}

function ConfigureLogging() {
	LOG_DIR="/mnt/bt/log"
	LOGFILE="$LOG_DIR"/"$TR_TORRENT_NAME".log
	if [ -f "$LOGFILE" ]; then
		rm "$LOGFILE"
	fi	

	touch "$LOGFILE"
	edate "ConfigureLogging - New Logfile created"
}

function RemoveTorrent() {
	if [ ! "$TR_TORRENT_ID" = "" ]; then
		transmission-remote --auth=$TRANS_AUTH --torrent $TR_TORRENT_ID --stop
    	transmission-remote --auth=$TRANS_AUTH --torrent $TR_TORRENT_ID --remove
    	if [ $? = 0 ];then
    		edate "RemoveTorrent - Torrent removed"
    	else
    		edate "RemoveTorrent - Failed to remove torrent - continuing processing"
    	fi
    else
    		edate "RemoveTorrent - Manual mode no removal"
    fi			
}

function CheckTorrentStructure() {
	if [ ! -d "$TR_TORRENT_DIR"/"$TR_TORRENT_NAME" ]; then
		edate "CheckTorrentStructure - Torrent directly in root directory - fixing"
		unset TR_DOWNLOADS
		local TR_TORRENT_FILE_NAME=$(basename "$TR_TORRENT_NAME")
		local TR_TORRENT_FOLDER_NAME="${TR_TORRENT_FILE_NAME%.*}"
		edate "CheckTorrentStructure - Torrent Name is $TR_TORRENT_FOLDER_NAME"
		mkdir "$TR_TORRENT_DIR"/"$TR_TORRENT_FOLDER_NAME"
		TR_DOWNLOADS="$TR_TORRENT_DIR"/"$TR_TORRENT_FOLDER_NAME"
		edate "CheckTorrentStructure - created folder $TR_DOWNLOADS"
		mv "$TR_TORRENT_DIR"/"$TR_TORRENT_FILE_NAME" "$TR_DOWNLOADS"/"$TR_TORRENT_FILE_NAME"
		edate "CheckTorrentStructure - Moved file $TR_TORRENT_FILE_NAME into folder $TR_DOWNLOADS"
	else
		edate "CheckTorrentStructure - Torrent with correct structure - no action needed"
	fi
}

function ScrubData() {
	for fname in "$TR_DOWNLOADS"/* 
	do
		if [ -f "$fname" ]; then
			edate "ScrubData - evaluating file $(basename "$fname")"

			case "${fname##*.}" in
				"mkv"|"m4v"|"mp4"|"avi")
				if [ $(stat -c %s "$fname") -lt "104857600" ]; then
					if [ ! -d "$TR_DOWNLOADS"/nomedia ]; then
						edate "ScrubData -  Create nomedia folder for temp storage of non media files"
						mkdir "$TR_DOWNLOADS"/nomedia
					fi	
					edate "ScrubData - Removed file $(basename "$fname") - Too Small less than 100MB"
					#mv "$fname" "$TR_DOWNLOADS"/nomedia/"$(basename "$fname")"
					rm "$fname"
				fi	
				edate "ScrubData - will keep file Video $(basename "$fname") for processing"
				;;
				"flac" | "m4a")
				edate "ScrubData - will keep file Music $(basename "$fname") for processing"
				;;
				*)
					if [ -f "$fname" ]; then
						if [ ! -d "$TR_DOWNLOADS"/nomedia ]; then
							edate "ScrubData -  Create nomedia folder for temp storage of non media files"
							mkdir "$TR_DOWNLOADS"/nomedia
						fi	
						#mv "$fname" "$TR_DOWNLOADS"/nomedia/"$(basename "$fname")"
						rm "$fname"
						edate "ScrubData - Removed file $(basename "$fname") - Not video file"
					fi;
				;;
			esac
			case $fname in
				*sample*|*SAMPLE*|*Sample*|*trailer*|*TRAILER*|*Trailer*|*deleted.scenes*|\
				*DELETED.SCENES*|*Deleted.Scenes*|*Deleted.scenes|*Extras*|*EXTRAS|*extras*)
					if [ -f "$fname" ]; then
						mv "$fname" "$TR_DOWNLOADS"/nomedia/"$(basename "$fname")"
						edate "ScrubData - Removed file $(basename "$fname") - Not interesting content"
					fi;
					;;
			esac 	
		fi
	done
}

function MediaQuality() {
	quality="1"
	case "$(echo $1 | sed -n 's/.*\([.| ][Hh][Dd][Tt][Vv][.| ]*\).*/\1/p')" in
	' HDTV '|'.HDTV.')
		quality="2"
		;;
		*)
		;;
	esac
	case "$(echo $1 | sed -n 's/.*\([.| ][1][0][8][0][pP][.| ]*\).*/\1/p')" in
	' 1080p ' | ' 1080P ' | '.1080p.' | '.1080P.')
		quality="2"
		;;
		*)
		;;
	esac	
	case "$(echo $1 | sed -n 's/.*\([.| ][7][2][0][pP][.| ]*\).*/\1/p')" in
	' 720p ' | ' 720P ' | '.720p.' | '.720P.')
		quality="2"
		;;
		*)
		;;
	esac

	edate "mediaquality - Quality set to $quality"
}

function SetMediaType() {
	REGEX="([sS]([0-9]{2,}|[X]{2,})[eE]([0-9]{2,}|[Y]{2,}))"
    	if [[ "$TR_TORRENT_NAME" =~ $REGEX ]]; then
        	mediatype="TVSHOW"
    	else
       		mediatype="MOVIE"
    	fi
    edate "setmediatype - Media is $mediatype"	
}

 function RenameTV() {
 	edate "RenameTV - Renaming TV Show file "$filename".m4v"
	filebot -rename "$TR_DOWNLOADS"/"$filename".m4v --format "{n}/{e.pad(2)} {t} ({sdhd})" --action copy -non-strict --output "$TARGET_DIR/TV Shows" --db "TheTVDB"
}

 function RenameMovie() {
 	edate "RenameMovie - Renaming Movie file "$filename".m4v"
	filebot -rename "$TR_DOWNLOADS"/"$filename".m4v --format "{n} ({sdhd})" --action copy -non-strict --output "$TARGET_DIR/Movies" --db "TheMovieDB"
}

function ParseTVMetaData() {
	
	edate "ParseTVMetaData - getting TVinfo"
	filebot -script "$WORKING_DIR"/tvinfo.groovy "$TR_DOWNLOADS" -non-strict
	if [ -f "$TR_DOWNLOADS"/tvshow.nfo ]; then
		edate "ParseTVMetaData - detecting media quality (SD or HD)"
		MediaQuality "$TR_DOWNLOADS"/"$filename".m4v
		TVShowName=`xmlstarlet sel -t -v "/tvshow/title" "$TR_DOWNLOADS"/tvshow.nfo`
		edate "ParseTVMetaData - TV Show Name is: $TVShowName"
		TVShowID=`xmlstarlet sel -t -v "/tvshow/id" "$TR_DOWNLOADS"/tvshow.nfo`
		edate "ParseTVMetaData - TV Show ID is: $TVShowID"
		TVSeasonNum=`echo "$TR_DOWNLOADS"/"$filename".m4v | sed -n 's/.*\([sS][0-9][0-9]*\).*/\1/p' | tr -d 'S' | tr -d 's'`
		edate "ParseTVMetaData - TV Show Season is: $TVSeasonNum"
		TVEpisode=`echo "$TR_DOWNLOADS"/"$filename".m4v | sed -n 's/.*\([eE][0-9][0-9]*\).*/\1/p' | tr -d 'E' | tr -d 'e'`
		edate "ParseTVMetaData - TV Show Episode is: $TVEpisode"
		edate "ParseTVMetaData - Getting TV Series Info to parse"
		GetSeriesInfo "$TVShowID"
		edate "ParseTVMetaData - Getting TV Episode Info to parse"
		GetEpisodeInfo "$TVShowID" "$TVSeasonNum" "$TVEpisode"
		TVEpisodeDescription=`jq '.data[].overview' "$TR_DOWNLOADS"/episode.json | tr -d '"'`
		edate "ParseTVMetaData - Episode Description is $TVEpisodeDescription"
		TVEpisodeTitle=`jq '.data[].episodeName' "$TR_DOWNLOADS"/episode.json | tr -d '"'`
		edate "ParseTVMetaData - Episode Description is $TVEpisodeTitle"
		TVEpisodeReleaseDate="`jq '.data[].firstAired' "$TR_DOWNLOADS"/episode.json`T00:00:00Z"
		edate "ParseTVMetaData - Episode Release Date is $TVEpisodeReleaseDate" 
		TVSerieGenre=`jq '.data.genre[]' "$TR_DOWNLOADS"/series.json | tr -d '"'`
		edate "ParseTVMetaData - Episode Genre is $TVSerieGenre"

		edate "ParseTVMetaData - Embedding metadata with AtomicParsley"
		AtomicParsley "$TR_DOWNLOADS"/"$filename".m4v\
 		--stik "TV Show"\
 		--artwork "$TR_DOWNLOADS"/poster.jpg\
 		--TVShowName "$TVShowName"\
 		--TVSeasonNum "$TVSeasonNum"\
 		--TVEpisodeNum "$TVEpisode"\
		--longdesc "$TVEpisodeDescription"\
		--albumArtist "$TVShowName"\
		--tracknum "$TVEpisode"\
		--title "$TVEpisodeTitle"\
		--hdvideo "$quality"\
		--year "$TVEpisodeReleaseDate"\
		--genre "$TVSerieGenre"\
		--overWrite
		edate "ParseTVMetaData - TV Show Metadata embedded"
	else
 		edate "ParseTVMetaData - Unable to obtain TV Show Meta data - exiting"
 		exit 1
 	fi			
}

function ParseMovieMetaData() {
	 
	edate "ParseMovieMetaData - getting Movie info"
	filebot -script "$WORKING_DIR"/movieinfo.groovy "$TR_DOWNLOADS" -non-strict
	if [ -f "$TR_DOWNLOADS"/movie.nfo ]; then
		edate "ParseMovieMetaData - detecting media quality (SD or HD)"
		MediaQuality "$TR_DOWNLOADS"/"$filename".m4v
		MovieName=`xmlstarlet sel -t -v "/movie/title" "$TR_DOWNLOADS"/movie.nfo`
		edate "ParseMovieMetaData - Movie Name is: $MovieName"
		MovieReleaseDate=`xmlstarlet sel -t -v "/movie/year" "$TR_DOWNLOADS"/movie.nfo`
		edate "ParseMovieMetaData - Movie Year is: $MovieReleaseDate"
	
		MovieGenre=`xmlstarlet sel -t -v "movie/genre[position() = 1]" "$TR_DOWNLOADS"/movie.nfo`
	
		if [ "$MovieGenre" = "" ]; then
			MovieGenre="Drama"
			edate "ParseMovieMetaData - No movie genre detected setting default to Drama"
		else 
			edate "ParseMovieMetaData - Movie Genre is: $MovieGenre"
		fi
	
		MovieDescription=`xmlstarlet sel -t -v "/movie/plot" "$TR_DOWNLOADS"/movie.nfo`
		if [ "$MovieDescription" = "" ]; then
			MovieDescription="No plot available"
		fi		

		edate "ParseMovieMetaData - Getting enhanced iTunes metadata"

		EnhancedMovieMeta
		
		edate "ParseMovieMetaData - Embedding Movie metadata with AtomicParsley"

		AtomicParsley "$TR_DOWNLOADS"/"$filename".m4v\
		--stik "Movie"\
		--artwork "$TR_DOWNLOADS"/poster.jpg\
 		--title "$MovieName"\
 		--hdvideo "$quality"\
 		--genre "$MovieGenre"\
 		--longdesc "$MovieDescription"\
 		--year "$MovieReleaseDate"\
 		--rDNSatom "$XMLMeta" name=iTunMOVI domain=com.apple.iTunes\
  		--overWrite
 	
 		edate "ParseMovieMetaData - Completed Embedding Movie Metadata"
 	else
 		edate "ParseMovieMetaData - Unable to obtain Movie Meta data - exiting"
 		exit 1
 	fi		
}

function EnhancedMovieMeta() {
	edate "EnhancedMovieMeta - Starting generate XML for enhanced iTunes metadata"
	XMLMeta='<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE plist PUBLIC \"-//Apple Computer//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.$<plist version=\"1.0\"><dict>'
	XMLMeta="$XMLMeta <key>copy-warning</key><string>FBI Anti-Piracy Warning: Unauthorized copying is punishable under federal law</string>"

	MovieStudio=`xmlstarlet sel -t -v "movie/studio[position() = 1]" "$TR_DOWNLOADS"/movie.nfo`
		if [ ! "$MovieStudio" = "" ]; then
			XMLMeta="$XMLMeta <key>studio</key><string>$MovieStudio</string>"
		fi

	MovieActors=`xmlstarlet sel -t -m "/movie/actor" -v name -o "," "$TR_DOWNLOADS"/movie.nfo`
		if [ ! "$MovieActors" = "," ]; then
		XMLMeta="$XMLMeta <key>cast</key><array>"
		SAVE_IFS=IFS
		IFS=","
		for i in $MovieActors
        	do
        		XMLMeta="$XMLMeta <dict><key>name</key><string>$i</string></dict>"
        	done
		XMLMeta="$XMLMeta </array>" 
		IFS=SAVE_IFS
		fi

	MovieProducers=`xmlstarlet sel -t -m "/movie" -v producer -o "," "$TR_DOWNLOADS"/movie.nfo`
		if [ ! "$MovieProducers" = "," ]; then
		XMLMeta="$XMLMeta <key>producers</key><array>"
		SAVE_IFS=IFS
		IFS=","
		for i in $MovieProducers
        	do
        		XMLMeta="$XMLMeta <dict><key>name</key><string>$i</string></dict>"
        	done
		XMLMeta="$XMLMeta </array>"
		fi

	MovieDirectors=`xmlstarlet sel -t -m "/movie" -v director -o "," "$TR_DOWNLOADS"/movie.nfo`
		if [ ! "$MovieDirectors" = "," ]; then
		XMLMeta="$XMLMeta <key>directors</key><array>"
		SAVE_IFS=IFS
		IFS=","
		for i in $MovieDirectors
        	do
        	XMLMeta="$XMLMeta <dict><key>name</key><string>$i</string></dict>"
        	done
		XMLMeta="$XMLMeta </array>"
		fi			
	XMLMeta="$XMLMeta </dict></plist>"	

	edate "EnhancedMovieMeta - Completed XML for iTunes enhanced metadata"
}
function LogonTVDB() {
	if [ -f "$TR_TORRENT_DIR"/token.json ]; then
		rm "$TR_TORRENT_DIR"/token.json
	fi
	curl -s -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{
		"apikey": "'"$TVDB_APIKEY"'",
		"username": "'"$TVDB_USERNAME"'",
		"userkey": "'"$TVDB_USERKEY"'"
	}' 'https://api.thetvdb.com/login' > $TR_TORRENT_DIR/token.json
	edate "LogonTVDB - New Logon and Token obtained from TheTVDB"
}

function RefreshToken() {
	if [ -f "$TR_TORRENT_DIR"/token.json ]; then
		token=`jq '.token' "$TR_TORRENT_DIR"/token.json | tr -d '"'`	#Check if existing token is valid
		curl -s -X GET -H 'Accept: application/json' -H "Authorization: Bearer $token" \
		'https://api.thetvdb.com/refresh_token' > "$TR_TORRENT_DIR"/token.json	
 			if [ "$(jq '.Error' "$TR_TORRENT_DIR"/token.json | tr -d '"')" = 'Not authorized' ]; then
			edate "RefreshToken - Token has expired - logging on again"
			LogonTVDB
			else
			edate "RefreshToken - Token is valid & refreshed"
			fi
	else
		edate "RefreshToken - No token found - logging on again"
		LogonTVDB
	fi		
}

function GetSeriesInfo() {
	edate "GetSeriesInfo - Refreshing TheTVDB Token"
	RefreshToken
	seriesid="$1"
	token=`jq '.token' "$TR_TORRENT_DIR"/token.json | tr -d '"'`
	if [ -f "$TR_DOWNLOADS"/series.json ]; then
		rm "$TR_DOWNLOADS"/series.json
	fi	
	curl -s -X GET --header 'Accept: application/json' --header "Authorization: Bearer $token" \
	"https://api.thetvdb.com/series/$seriesid" > "$TR_DOWNLOADS"/series.json
	if [ -f "$TR_DOWNLOADS"/series.json ]; then
		edate "GetSeriesInfo - series.json written to disk and available for processing"
		else
		edate "failed to obtain data from TheTVDB and write series.json"
	fi	
}

function GetEpisodeInfo() {
	edate "GetEpisodeInfo - Refreshing TheTVDB Token"
	RefreshToken
	seriesid="$1"
	seasonid="$2"
	episodeid="$3"

	token=`jq '.token' "$TR_TORRENT_DIR"/token.json | tr -d '"'`
	if [ -f "$TR_DOWNLOADS"/episode.json ]; then
		rm "$TR_DOWNLOADS"/episode.json
	fi	
	curl -s -X GET --header 'Accept: application/json' --header "Authorization: Bearer $token" \
 	"https://api.thetvdb.com/series/$seriesid/episodes/query?airedSeason=$seasonid&airedEpisode=$episodeid" > "$TR_DOWNLOADS"/episode.json
 	if [ -f "$TR_DOWNLOADS"/episode.json ]; then
		edate "GetEpisodeInfo - episode.json written to disk and available for processing"
	else
		edate "failed to obtain data from TheTVDB and write episode.json"
	fi
}

function CleanUp() {
	rm -fr "$TR_DOWNLOADS"
	edate "cleanup - Removing source folder $TR_DOWNLOADS"
}

function CreateMediaInfoXML() {
	if [ -f "$TR_DOWNLOADS"/mediainfo.xml ]; then
		edate "CreateMediaInfoXML - removing existing file before generating new"
		rm 	"$TR_DOWNLOADS"/mediainfo.xml
	fi
		edate "CreateMediaInfoXML - Creating XML file mediainfo.xml for file $1"
		mediainfo --output=XML "$1" >> "$TR_DOWNLOADS"/mediainfo.xml		
	
	if [ -f "$TR_DOWNLOADS"/mediainfo.xml ]; then
		edate "CreateMediaInfoXML - Created xml file successfully"
	else
		edate "CreateMediaInfoXML - failed to create mediainfo.xml for $1 - exiting"
		exit 1
	fi			
}

function ConvertFile() {
	filename=$(basename "$1")
	filename="${filename%.*}"
	edate "ConvertFile - Converting file $(basename "$1")"

	case `xmlstarlet sel -t -v "/Mediainfo/File/track[@type='Video']/Format" "$TR_DOWNLOADS"/mediainfo.xml` in
        'AVC')
                param_video="-c:v copy"
                ;;
        'HEVC' | 'MPEG-4 Visual')
                param_video="-c:v libx264"
                ;;
     	'')
     			param_video=""       
                ;;
                *)
                param_video="-c:v libx264"
                ;;
	esac

	case `xmlstarlet sel -t -v "/Mediainfo/File/track[@type='Audio']/Format" "$TR_DOWNLOADS"/mediainfo.xml` in
        'AAC')
                param_audio="-c:a copy"
                ;;
        'DTS' | 'AC-3')
                param_audio="-c:a libfdk_aac -b:a 768k -cutoff 18000"
        'FLAC'
        		param_audio="-c:a alac"	        
                ;;
                *)
                param_audio="-c:a libfdk_aac -b:a 768k -cutoff 18000"
                ;;
	esac
	edate "ConvertFile - converting with Video Parameter $param_video"
	edate "ConvertFile - converting with Audio Parameter $param_audio"
	if [ ! "$param_video" = "" ]; then
	/usr/bin/ffmpeg -i "$1" ${param_video} ${param_audio} "$TR_DOWNLOADS"/"$filename".m4v
	else
	/usr/bin/ffmpeg -i "$1" ${param_audio} "$TR_DOWNLOADS"/"$filename".m4a
	fi	
	if [ $? = 0 ]; then
		edate "ConvertFile - Conversion completed"
		edate "ConvertFile - Deleting source file $(basename "$1")"
		rm "$1"
	else
		edate "ConvertFile - conversion failed exiting"
		exit 1
	fi	
}

function ProcessFile() {
	edate "ProcessFile - Starting processing of media file $(basename "$f")"
	filename=$(basename "$1")
	extension="${filename##*.}"
	filename="${filename%.*}"
	CreateMediaInfoXML "$1"
	edate "$extension"
	case "$extension" in 
		"mp4") edate "ProcessFile - Detected .mp4 file"
		ConvertFile "$1"
		if [ "$mediatype" = "TVSHOW" ]; then
			ParseTVMetaData
			RenameTV
		else	
			ParseMovieMetaData
			RenameMovie
		fi 
			;;

		"m4v") edate "ProcessFile - Detected .m4v file"
		if [ "$mediatype" = "TVSHOW" ]; then
			ParseTVMetaData
			RenameTV
		else	
			ParseMovieMetaData
			RenameMovie
		fi
			;;

		"mkv") edate "ProcessFile - Detected .mkv file - converting to ITunes format"
		ConvertFile "$1"
		if [ "$mediatype" = "TVSHOW" ]; then
			ParseTVMetaData
			RenameTV
		else
			ParseMovieMetaData 
			RenameMovie
		fi
			;;

		"avi") edate "ProcessFile - Detected .avi - converting to ITunes format"
		ConvertFile "$1"
		if [ "$mediatype" = "TVSHOW" ]; then
			ParseTVMetaData
			RenameTV
		else
			ParseMovieMetaData 
			RenameMovie
		fi
			;;
		*) edate "ProcessFile - No action for $1 unknown file"
			#exit 1
			;;	
	esac
}


	#**** Script Processing Starts Here
	#exit 0 #temporarily disable auto processing
	if [ ! "$1" = "" ]; then
		TR_TORRENT_NAME="$1"
		if [ -d "$TR_TORRENT_DIR"/"$1" ] || [ -f "$TR_TORRENT_DIR"/"$1" ]; then
			TR_DOWNLOADS="$TR_TORRENT_DIR"/"$TR_TORRENT_NAME"
			ConfigureLogging
			edate "Manual Processing detected"
		else
			echo "Directory or file in argument does not exist - exiting"
			exit 1
		fi	
	else
		ConfigureLogging
		edate "Automatic Processing detected"
	fi
	
	if [ ! "$DEBUG" = 0 ]; then
	edate "info - *********START DEBUG INFO**********************"
	edate "info - current Transmission user is $TRANSMISSION_USER"
	edate "info - Home directory for $TRANSMISSION_USER is $HOME"
	edate "info - Java Path is $JAVA_DIR"
	edate "info - Directory is $TR_TORRENT_DIR"
	edate "info - Torrent ID is $TR_TORRENT_ID"
	edate "info - Torrent Hash is $TR_TORRENT_HASH"
	edate "info - *********END DEBUG INFO************************"
	edate ""
	edate ""
	fi
	edate "init - Starting Processing"
	edate "init - Removing Torrent"
		RemoveTorrent
	edate "init - Checking if torrent is in directory or root"
		CheckTorrentStructure
	edate "init - Working on the new download $TR_DOWNLOADS"
	edate "init - scrubbing data"
		ScrubData
	edate "init - Detect Media Type TV OR Movie"
		SetMediaType

	#Main loop
	for f in "$TR_DOWNLOADS"/*
		do
			if [ -f "$f" ]; then
				ProcessFile "$f"
			fi
		done	
	edate "exit - starting cleanup"
		#CleanUp
	edate "exit - Processing completed for $TR_TORRENT_NAME"
